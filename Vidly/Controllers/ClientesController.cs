﻿using System.Linq;
using System.Web.Mvc;
using Vidly.Models;
using System.Data.Entity;
using Vidly.ViewModels;

namespace Vidly.Controllers
{

    public class ClientesController : Controller
    {
        private BDContexto _context;

        public ClientesController()
        {
            _context = new BDContexto();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult ListarClientes()
        {
            var clientes = _context.Clientes.Include(c => c.TipoDeMembro).ToList();

            return View(clientes);
        }

        public ActionResult ClienteForm()
        {
            var tpDeMembro = _context.TipoDeMembro.ToList();

            var viewModel = new ClienteFormViewModel()
            {
                Cliente = new Cliente(),

                TipoDeMembro = tpDeMembro
            };

            return View(viewModel);
        }

        public ActionResult NovoCliente()
        {
            var tpDeMembro = _context.TipoDeMembro.ToList();

            var viewModel = new ClienteFormViewModel()
            {
                Cliente = new Cliente(),

                TipoDeMembro = tpDeMembro
            };

            return View("ClienteForm", viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarCliente(Cliente cliente)
        {

            if (!ModelState.IsValid)
            {
                var viewModel = new ClienteFormViewModel
                {
                    Cliente = cliente,
                    TipoDeMembro = _context.TipoDeMembro.ToList()
                };

                return View("ClienteForm", viewModel);
            }

            if (cliente.Id == 0)
            {
                _context.Clientes.Add(cliente);
            }
            else
            {
                var clienteInBd = _context.Clientes.SingleOrDefault(c => c.Id == cliente.Id);

                clienteInBd.DataDeNascimento = cliente.DataDeNascimento;
                clienteInBd.InscritoParaNovasNoticias = cliente.InscritoParaNovasNoticias;
                clienteInBd.Nome = cliente.Nome;
                clienteInBd.TipoDeMembroId = cliente.TipoDeMembroId;
            }

            _context.SaveChanges(); // salva na base de dados

            return RedirectToAction("ListarClientes", "Clientes");


        }

        public ActionResult EditarCliente(int id)
        {
            var cliente = _context.Clientes.SingleOrDefault(c => c.Id == id);

            if (cliente == null)
                return HttpNotFound();

            var viewModel = new ClienteFormViewModel
            {
                Cliente = cliente,
                TipoDeMembro = _context.TipoDeMembro.ToList()
            };

            return View("ClienteForm", viewModel);

        }

        public ActionResult DetalharClientes(int id)
        {
            var cliente = _context.Clientes.Include(c => c.TipoDeMembro).SingleOrDefault(c => c.Id == id);

            if (cliente == null)
                return HttpNotFound();

            return View(cliente);
        }
    }
}