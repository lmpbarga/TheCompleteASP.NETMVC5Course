﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Filme
    {
        public Filme()
        {
            DataDeAdicao = DateTime.Parse("01/01/1900");
            DataDeLancamento = DateTime.Parse("01/01/1900");
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        public GeneroDoFilme GeneroDoFilme { get; set; }

        [Required]
        [Display(Name = "Gênero do Filme")]
        public int GeneroDoFilmeId { get; set; }

        [Required]
        [Display(Name = "Data de Lançamento")]
        public DateTime DataDeLancamento { get; set; }

        [Required]
        [Display(Name = "Data de Adição")]
        public DateTime DataDeAdicao { get; set; }

        [Required]
        [Range(1, 20)]
        [Display(Name = "Número em Estoque")]
        public int NumeroEmEstoque { get; set; }

    }
}