﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Min18Anos : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var cliente = (Cliente)validationContext.ObjectInstance;

            if (cliente.TipoDeMembroId == TipoDeMembro.Unknown || 
                cliente.TipoDeMembroId == TipoDeMembro.Mensal)
                return ValidationResult.Success;

            if (cliente.DataDeNascimento == null)
                return new ValidationResult("Data de Nascimento é necessária.");

            var idade = DateTime.Today.Year - cliente.DataDeNascimento.Value.Year;

            return (idade >= 18) 
                ? ValidationResult.Success 
                : new ValidationResult("Cliente deve ter apartir de 18 anos para ser um membro.");
        }
    }
}