﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class ClienteFormViewModel
    {
        public IEnumerable<TipoDeMembro> TipoDeMembro  { get; set; }
        public Cliente Cliente { get; set; }
    }
}