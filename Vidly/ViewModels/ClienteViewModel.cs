﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class ClienteViewModel
    {
        public Cliente Cliente { get; set; }
        public List<Cliente> Clientes { get; set; }

    }
}